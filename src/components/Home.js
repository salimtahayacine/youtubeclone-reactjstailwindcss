import React from "react";
import ReactDOM from "react-dom";
import useFetch from "./useFetch";
import Navbar from "./Navbar";
import Siderbar from "./Siderbar";
import Videos from "./Videos";
import SecondBar from "./SecondBar";
//import {BrowserRouter as Router,Routes,Route} from 'react-router-dom';
function Home() {
    //const sidebar = document.querySelector('#sidebar');
    //const {data:VideoInfos ,isPending , error} = useFetch('http://localhost:8000/VideoInfos');
    // const {data:Shorts } = useFetch('http://localhost:8000/Shorts');
  return (
     <div className="App">
      <div className="flex  ">
        <Siderbar  /> 
        <div className="">
          <SecondBar />
          {<Videos  />}
        </div>
      </div>
    </div>
 
  );
}

export default Home