import React from 'react'


function ItemYoutube(props) {
  return (
    <div>
          <div className='menusidebar flex justify-start items-center m-1 p-1 hover:bg-gray-200 hover:cursor-pointer '>
            <div className='img-yt'>
              {props.imge}
            </div>
               <h2 className=' mx-3'>{props.title}</h2>
           </div>
    </div>
  )
}

export default ItemYoutube