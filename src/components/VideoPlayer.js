import React from 'react'
import Navbar from './Navbar'
import useFetch from './useFetch'
import { useNavigate,useParams } from 'react-router-dom';
import Siderbar from './Siderbar';
import ReactPlayer from 'react-player';
import {VideoDatas,ShortDatas} from './data'
import SecondBar from './SecondBar';
function VideoPlayer() {
    //const { id } = useParams();
    //const {data: VideoInfo} = useFetch('http://localhost:8000/VideoInfos/'+ id);

    //const navigate = useNavigate();
  return (
    <div>
        <Navbar />
        <SecondBar />
        <div className=''>
        <div className='VideoPlayer '>
          {<div className='m-14'>
            {/* <iframe width="1122" height="631" src={"https://www.youtube.com/embed/yYvKJ4YrTw0"} title="AFRO DEEP HOUSE | black coffee | caiiro | mix by Zak" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> */}
              <ReactPlayer
                                            url={VideoDatas.url}
                                            height="631px"
                                            width="1122px"
                                            controls
                                            /> 
           <h2 className=' m-2 text-xl'>{VideoDatas.title}</h2>
           <h2 className=' m-2 text-sm'>{VideoDatas.Views} . {VideoDatas.DateUpload}</h2>
           <p className=' m-2 text-sm'>{VideoDatas.description}</p>
           
            
            </div>}

        </div>
        </div>

    </div>
  )
}

export default VideoPlayer