import { useEffect,useState } from "react";
const useFetch = (url) =>
{
    const [data,setData] = useState(null);
   // const [isPending,setISPending] = useState(true);
    //const [error,setError] = useState();

    useEffect( () => {
        //const abortCont = new AbortController();
            fetch (url)
            .then(res => {
                if(!res.ok)
                {
                   // throw Error ('Could not Fetch the data for the resources');
                }
                return res.json();
            }).then (data => {
                //console.log(data);
                setData(data);
                //setError(null);
            })
            // .catch ( err => {
            //     if(err.name === 'AbortError')
            //     { console.log('fetch Aborted')}
            //     else{
            //         //setError(err.message); 
            //     }
            // })
        

       //return () => abortCont.abort();
    },[url]);
    return{data}
}
export default useFetch;