import React from "react";
import ReactPlayer from "react-player";
import useFetch from "./useFetch";
import {useParams , useNavigate} from 'react-router-dom';

const Video = () => {

   const { id } = useParams() ;
   const {data: VideoInfo} = useFetch('http://localhost:8000/VideoInfos/' +id);
   const {data: Shorts } = useFetch('http://localhost:8000/Shorts/' +id);
   const navigate = useNavigate();
    return ( 
        <div className="Videoinfos ">
                 {/* <img className="w-80 h-40" src={props.VideoThambnail}></img> */}
                {VideoInfo && 
                        <div className=''>
                                <div>
                                        {/* <ReactPlayer
                                        url={VideoInfo.url}
                                        height="170px"
                                        width="320px"
                                        controls
                                        /> */}
                                   <img className="" src={VideoInfo.thambnails}></img> 
                                </div>
                                <div className="">
                                <div className="title-video m-1 flex items-center">
                                    <img className='w-8 rounded-full' src={VideoInfo.chaineAvatar}></img>
                                    <h1 className="p-1">{VideoInfo.title}</h1>
                                
                                </div>
                                <div className="mx-9 text-gray-600 text-sm">
                                   <span>{VideoInfo.chaineName}</span>
                                </div>
                                <div className='Video-infos flex items-center mx-7 text-sm'>
                                  <span className="px-2 text-gray-600 text-sm">{VideoInfo.Views} | {VideoInfo.DateUpload}</span>
                                    
                                </div>
                        </div>
                 </div>}
                 {Shorts && <div className='thamb-video border  m-1 p-1'>
                                <div>
                                     <ReactPlayer
                                        url={Shorts.url}
                                        height="170px"
                                        width="320px"
                                        controls
                                        /> 
                                    
                                </div>
                                <div className="">
                                <div className="title-video m-1 flex items-center">
                                    <h1 className="p-1">{Shorts.title}</h1>
                                
                                </div>
                                <div className='Video-infos flex items-center mx-7 text-sm'>
                                  <span className="px-2 text-gray-600 text-sm">{VideoInfo.Views}</span>
                                    
                                </div>
                        </div>
                 </div>}
    
            

        </div>
     );
}
 
export default Video;