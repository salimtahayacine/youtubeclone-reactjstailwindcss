import React from "react";
import  ReactDOM  from "react-dom";
import './index.css'
// import Navbar from "./components/Navbar";
// import SecondBar from "./components/SecondBar";
// import Siderbar from "./components/Siderbar";
//import useFetch from "./components/useFetch";
// import Videos from "./components/Videos";
import {BrowserRouter as Router ,Route,Routes} from 'react-router-dom';
import Home from "./components/Home";
import VideoPlayer from "./components/VideoPlayer";
import Navbar from "./components/Navbar";
import Siderbar from "./components/Siderbar";
import {VideoDatas} from './components/data'


function App() {

 //const {data:VideoInfo ,isPending , error} = useFetch('http://localhost:8000/VideoInfo');
//  const {data:Shorts } = useFetch('http://localhost:8000/Shorts');

  return (
   <Router>
    <Navbar />
    
     <Routes>
       <Route path="/" element={<Home/>}></Route>
       <Route path='/VideoDatas/:id' element={<VideoPlayer/>}></Route>
     </Routes>
   </Router>
  );
}

export default App;
